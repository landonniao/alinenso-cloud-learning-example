package com.neo.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@SuppressWarnings("serial")
@Entity
public class SysPermission implements Serializable {
	private Boolean available = Boolean.FALSE;
	@Id
	@GeneratedValue
	private Integer id;// 主键.
	private String name;// 名称.
	private Long parentId; // 父编号
	private String parentIds; // 父编号列表
	private String permission; // 权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view
	@Column(columnDefinition = "enum('menu','button')")
	private String resourceType;// 资源类型，[menu|button]
	@ManyToMany
	@JoinTable(name = "SysRolePermission", joinColumns = { @JoinColumn(name = "permissionId") }, inverseJoinColumns = {
			@JoinColumn(name = "roleId") })
	private List<SysRole> roles;
	private String url;// 资源路径.

	public Boolean getAvailable() {
		return available;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Long getParentId() {
		return parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public String getPermission() {
		return permission;
	}

	public String getResourceType() {
		return resourceType;
	}

	public List<SysRole> getRoles() {
		return roles;
	}

	public String getUrl() {
		return url;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public void setRoles(List<SysRole> roles) {
		this.roles = roles;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}