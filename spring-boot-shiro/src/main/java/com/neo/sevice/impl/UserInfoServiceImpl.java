package com.neo.sevice.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.neo.dao.UserInfoDao;
import com.neo.entity.UserInfo;
import com.neo.sevice.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Resource
    private UserInfoDao userInfoDao;
    @Override
    public UserInfo findByUsername(String username) {
        System.out.println("UserInfoServiceImpl.findByUsername()");
        return userInfoDao.findByUsername(username);
    }
}